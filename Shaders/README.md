# Shaders

## Final shaders :
- **Dust clouds modelisation** : [Spiral galaxy dust clouds](Spiral_galaxy_dust_clouds) (or [on shadertoy](https://www.shadertoy.com/view/lldczr))  
  TODO
- **Galaxy linearization** : [Spiral galaxy linearization](Spiral_galaxy_linearization) (or [on shadertoy](https://www.shadertoy.com/view/lsKcWc))  
  TODO


## Research steps :
- Find orthogonal parametrization to a spiral :
  - With strange spiral :
    - [Spiral distortion on shadertoy](https://shadertoy.com/view/XsfSzN) : an old attempt to make orthognal spiral mesh on strange Archimede spirals
    - [Spiral tangents&normals on shadertoy](https://www.shadertoy.com/view/lddcRs) : determining normals to the strange Archimede spirals
  - With logaritmic spiral :
    - [Spiral orthogonality on shadertoy](https://www.shadertoy.com/view/Xd3cWf) : determining normals to the logartihmic spiral, and getting the idea that the orthogonal curve to a log spiral is another log spiral
    - [Spiral regular orthogonal mesh on shadertoy](https://www.shadertoy.com/view/lsKyz1) : determining the orthonormal log spiral and conform number of arms
    - [Spiral orth parametrization on shadertoy](https://www.shadertoy.com/view/XdyyWw) : determining a conform parametrization of the log spiral
- Observation of dust clouds in parametric space :
  - **[Spiral galaxy linearization](Spiral_galaxy_linearization) (see above)**
- Dust clouds modelisation using [multiplicative Perlin noise](https://www.shadertoy.com/view/Xs23D3) :
  - [Noise experimentations on shadertoy](https://www.shadertoy.com/view/MddfRM) : studying various methods for the modelisation in veins
  - [Spiral galaxy noisy arms on shadertoy, versions 1](https://www.shadertoy.com/view/ldVcDm),[2](https://www.shadertoy.com/view/Xd3fzl),[3](https://www.shadertoy.com/view/ld3fWB)
  - **[Spiral galaxy dust clouds](Spiral_galaxy_dust_clouds) (see above)**