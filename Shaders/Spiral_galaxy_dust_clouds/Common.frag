
#define PI 3.141593
#define R iResolution


//////////////////////////////////////////////
// Keyboard controls and params tuning

#define keyToggle(K) (texture(keyboardChannel,vec2((.5+float(K))/256.,0.75)).x > 0.)


#define DISP_SLIDERS    	 keyToggle(_G)
#define LINEARIZE       	 keyToggle(_L)
#define FANCY_COL       	!keyToggle(_F)
#define DO_BULB         	!keyToggle(_B)
#define BULB_R .03
#define BULB_COMPR 3.
#define DO_SPURS        	!keyToggle(_S)
#define NOISE_ANISOTROPY	!keyToggle(_A)
#define RAW_DENSITY	    	 keyToggle(_O)

// Debug :
#define DISP_MESH       	 keyToggle(_M)
#define MESH_COMPR 13
#define DISP_NOISE      	 keyToggle(_N)
#define DISP_INFO       	 keyToggle(_D)
#define INVERT_COLORS   	 keyToggle(_I)


// Arm offset (currently needed because of the spurs discontinuity)
#define ARM_POS .3

////////////////////
// Sliders
#define SLIDER_W      	.025
#define SLIDER_H      	.2
#define SLIDER_SPACING	.025
#define SLIDER_LINE_W 	.01
// Center position of first slider
#define SLIDER_POS		vec2(.025,.2)
#define SLIDER_COLOR    RED

// i-th slider center
#define SliderPosFromI(i) (SLIDER_POS + float(i)*vec2(SLIDER_W+SLIDER_SPACING,0))
// slider index from position
// if index >= NB_SLIDERS, then position is beyond sliders
#define iSliderFromPos(pos) int((pos.x-SLIDER_POS.x+SLIDER_W/2.)/(SLIDER_W+SLIDER_SPACING))

// Ascii values of chars
const int	_=32,           // space
			_LPAR=40,		// (
			_RPAR=41,		// )
			_MINUS=45,		// -
			_DOT=46,		// .
    		_COLON=58,		// :
    		_EQ=61,			// =
			_0=48,
			_A=65,	_B=66,	_C=67,	_D=68,	_E=69,
			_F=70,	_G=71,	_H=72,	_I=73,	_J=74,
			_K=75,	_L=76,	_M=77,	_N=78,	_O=79,
			_P=80,	_Q=81,	_R=82,	_S=83,	_T=84,
			_U=85,	_V=86,	_W=87,	_X=88,	_Y=89,
			_Z=90,
			_a=97,	_b=98,	_c=99,	_d=100,	_e=101,
			_f=102,	_g=103,	_h=104,	_i=105,	_j=106,
			_k=107,	_l=108,	_m=109,	_n=110,	_o=111,
			_p=112,	_q=113,	_r=114,	_s=115,	_t=116,
			_u=117,	_v=118,	_w=119,	_x=120,	_y=121,
			_z=122;

// Sliders default values, and value factors
struct Slider{
    float def_value;
    float value_factor;
};
const Slider[] sliders =
    Slider[](
        Slider(.11,1./8.),
        Slider(.0 ,2.   ),
        Slider(.32,PI   ),
        Slider(.17,10.  ),
        Slider(.38,2.   ),
        Slider(.1 ,1./3.),
        Slider(.27,1.   ),
        Slider(.25,1.   ),
        Slider(.13,PI   ),
        Slider(.25,2.   ),
        Slider(.22,6.   ),
        Slider(.38,1.   ),
        Slider(.56,3.   )
    );

const int NB_SLIDERS = sliders.length();

// Labels of the sliders
// Shader is way faster by putting it in a separate array,
// rather than in the Slider structure
const int[] sliders_labels = int[](_A,_W,_,
                                   _L,_a,_,
                                   _S,_A,_,
                                   _S,_F,_,
                                   _S,_L,_,
                                   _S,_W,_,
                                   _D,_F,_,
                                   _D,_A,_,
                                   _A,_A,_,
                                   _A,_S,_,
                                   _O,_F,_,
                                   _L,_D,_,
                                   _H,_D);

// Pow functions
// cheaper than pow(x,y)
#define POW2(A) ((A)*(A))
#define POW3(A) (POW2(A)*(A))
#define POW4(A) (POW3(A)*(A))

// 2D rotation matrix
#define RMat2D(A) mat2(cos(A),-sin(A),sin(A),cos(A))

// Colors
#define BLACK		vec3(0)
#define WHITE		vec3(1)
#define RED  		vec3(1,0,0)
#define GREEN		vec3(0,1,0)
#define BLUE 		vec3(0,0,1)
#define TURQUOISE 	vec3(0,1,1)
#define YELLOW 		vec3(1,1,0)
#define LIGHT_BLUE	vec3(0,.5,1)
#define LAGOON		vec3(0,1,.5)
#define PINK		vec3(1,.3,.7)

float logBase(float base, float x){
    return log(x)/log(base);
}

// Periodic function __|__|__|__|__
// s : dirac compression
float periodicDirac(float x, float period, float s){
	return pow(abs(cos(x*(PI/period))),s);
}

// Point to rectangle distance
float dRect(vec2 p, vec2 center, vec2 size){
    return length(max(abs(p-center)-size/2.,0.));
}

///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
////////////////////// Font printing lib //////////////////////
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

struct PrintPosition{
    vec2 uv,
         pos;
    float scale;
};

struct PrintStyle{
    vec3 char_color,
         outline_color;
    float outline_size;
};

PrintStyle NewPrintStyle(vec3 col1, vec3 col2, float w){
	return PrintStyle(col1,col2,w);
}
PrintStyle NewPrintStyle(vec3 col1, vec3 col2){
	return PrintStyle(col1,col2,.05);
}
PrintStyle NewPrintStyle(vec3 col){
	return PrintStyle(col,col,0.);
}
#define DefaultPrintStyle NewPrintStyle(BLACK,YELLOW)

#define getDisplayPos(pos,scale) PrintPosition((fragCoord-pos*R.xy)/(scale*R.x),pos,scale);
#define nextLine(p)	p.pos.y -= scale*1.5,\
					p = getDisplayPos(p.pos,p.scale);

void writeChar(int char, PrintStyle style,
               float w, float x_offset, // Dimensions of the bbox of the char
               inout PrintPosition p, sampler2D font_tex, inout vec3 color){
    	#define getFont(uv,char) texture(font_tex, (uv+vec2(char%16,15-char/16))/16.)
		vec2 uv = p.uv;
    	uv.x += x_offset;
    	float outline_size = style.outline_size;
    	if(uv.x>0. && uv.x<w && uv.y>0. && uv.y<1.){
            // We are inside the bbox, display the char
            color = mix(color,style.outline_color,smoothstep(outline_size+.51,outline_size+.48,getFont(uv,char).a));
            color = mix(color,style.char_color, getFont(uv,char).r);
        }
		uv.x -= w; // move uv for next char
    	p.uv = uv;
}

const float default_ch_w = .8,
            default_ch_off = .3;

void writeStandardChar(int char, PrintStyle style,
                     inout PrintPosition p, sampler2D font_tex, inout vec3 color){
    writeChar(char,style,
              default_ch_w,default_ch_off,
              p,font_tex,color);
}

#define writeWord(word, style, p, font_tex, color)\
	for(int i_=0;i_<word.length();i_++)\
        writeStandardChar(word[i_],style,p,font_tex,color);



int powInt(int a, int b){
    int r = 1;
    for(int i=0;i++<b;r*=a);
    return r;
}

int intLog(int x, int base){
    if(x<1) return 0;
    int res = 0;
    for(;x>=base;res++)
        x /= base;
    return res;
}

void writeNumber(float number, int min_int_digits, int dec_digits,
                 PrintStyle style,
                 inout PrintPosition p, sampler2D font_tex, inout vec3 color){
	
    if(isnan(number)){
    	PrintStyle NaN_style = PrintStyle(BLACK,RED,.05);
        writeWord(int[](_N,_a,_N),NaN_style,p,font_tex,color);
    }
    else if(isinf(number)){
        PrintStyle Inf_style = PrintStyle(BLACK,LIGHT_BLUE,.05);
        writeWord(int[](_I,_n,_f),Inf_style,p,font_tex,color);
    }
    else{
        // Display the minus if number is negative
        if(number<0.)
            writeStandardChar(_MINUS,style,p,font_tex,color);
        
        // Round the number according to the number of decimal digits
        float decimal_digits_factor = float(powInt(10,dec_digits));
        int rounded_number = int(round(abs(number)*decimal_digits_factor));
        
        int int_part = rounded_number/int(decimal_digits_factor);
        int int_digits = 1 + intLog(int_part,10);
        // Fill with zeros to match min digits
        for(int i=0;i++< min_int_digits - int_digits ;)
            writeStandardChar(_0,style,
                              p,font_tex,color);
        
        
        int digits = int_digits+dec_digits;
        for(int x = powInt(10,digits);digits>0;digits--){
            if(digits==dec_digits)
                // Dot
                writeChar(_DOT,style,
                      .65,.45,
                      p,font_tex,color);
            writeStandardChar(_0+rounded_number/(x/=10),style,
                              p,font_tex,color);
            rounded_number%=x;
        }
    }
        
}

///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
///////////////// Multiplicative Perlin Noise /////////////////
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

// Fork of "Infinite Perlin Noise" by LoubetG. https://shadertoy.com/view/Xs23D3
// 2018-02-16 10:25:53

// --- Fractal noise simulating heterogeneous density in galactic clouds
// ---   -> help from Fabrice Neyret, https://www.shadertoy.com/user/FabriceNeyret2
// ---   -> noise functions from Inigo Quilez, https://www.shadertoy.com/view/XslGRr


// Number of computed scales
#define NbScales 22.

// Anti aliasing
#define LimitDetails 2.5
#define SmoothZone 100.

#define ZoomDistance 10.

// Size of the first Perlin Noise grid
#define FirstDivision 8.

#define GazConcentration 1

// Caracteristic ratio of the frequencies (0.5 for octaves)
#define fRatio .5

// --- noise functions from https://www.shadertoy.com/view/XslGRr
// Created by inigo quilez - iq/2013
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

// random hash
vec2 hash( vec2 p ) { // -> [-1,1]
	p = vec2( dot(p,vec2(127.1,311.7)),
			  dot(p,vec2(269.5,183.3)));
	return -1. + 2.*fract(sin(p+20.)*53758.545312);
}

// Tileable anisotropic noise doesnt work
// To be fixed
#define TILEABLE_NOISE 1
float noise( vec2 p, vec2 cycle ) { // -> [-1,1]
    vec2 i = floor(p),
         f = fract(p);
	vec2 u = smoothstep(0.,1.,f);
#if TILEABLE_NOISE
    return mix( mix( dot( hash( mod(i + vec2(0,0),cycle) ), f - vec2(0,0) ), 
                     dot( hash( mod(i + vec2(1,0),cycle) ), f - vec2(1,0) ), u.x),
                mix( dot( hash( mod(i + vec2(0,1),cycle) ), f - vec2(0,1) ), 
                     dot( hash( mod(i + vec2(1,1),cycle) ), f - vec2(1,1) ), u.x),
                u.y);
#else
    return mix( mix( dot( hash( i + vec2(0,0) ), f - vec2(0,0) ), 
                     dot( hash( i + vec2(1,0) ), f - vec2(1,0) ), u.x),
                mix( dot( hash( i + vec2(0,1) ), f - vec2(0,1) ), 
                     dot( hash( i + vec2(1,1) ), f - vec2(1,1) ), u.x),
                u.y);
#endif
}

// -----------------------------------------------

float cyclicMultNoise( vec2 uv, vec2 resolution, vec2 base_cycle ) {

	float d = 1.; // initial density
    
	float n_tiles_level_1 = pow(2.,FirstDivision+round(ZoomDistance/log(2.)));
    
    uv *= n_tiles_level_1;
	vec2 cycle = base_cycle*n_tiles_level_1;
    
	// computation of the multiplicative noise
	float q = n_tiles_level_1;
	for (float i = 0.; i < NbScales; i++) {
        // Stop if the value is too low (and we assume it will thus stay low)
		if (d<1e-2) continue;
		
		// compute only the visible scales
		float crit = q - length(resolution)/LimitDetails;
		if (crit < SmoothZone) {
            
            float n = noise(uv + 10.7*i*i, cycle); // n : [-1,1]

            // Sharpen the noise
            for (int j = 0; j < GazConcentration; j++) {
                n = sin(n*PI/2.); // n : [-1,1] -> [-1,1]
            }

            n = n+1.; // n : [-1,1] -> [0,2]
            
            if (crit>0.) {
                // avoid aliasing by linear interpolation
				float t = crit/SmoothZone;
				n = mix(n,1.,t);
			}
            
			d *= n;
		}
		
        // go to the next octave
		uv *= fRatio;
        cycle *= fRatio;
        q*= fRatio;
	}
	
	d = max(d,0.);
    return d;
}


///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
//////////////////// Additive Perlin Noise ////////////////////
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

#define FirstDiv 8.

float noise2( vec2 p, vec2 cycle )
{
    vec2 i = floor(p);
    vec2 f = fract(p);
    vec2 u = smoothstep(0.,1.,f);
    return mix( mix( hash( mod(i + vec2(0,0),cycle) ), 
                     hash( mod(i + vec2(1,0),cycle) ), u.x),
                mix( hash( mod(i + vec2(0,1),cycle) ), 
                     hash( mod(i + vec2(1,1),cycle) ), u.x), u.y).x;
}

float cyclicAddNoise( vec2 uv, int octaves, vec2 base_cycle){
    float f=0.;
    vec2 cycle = base_cycle*FirstDiv;
    uv *= FirstDiv;
    for(int i=0;i<octaves; i++){
        f += 1./(pow(2.,float(i+1)))*noise2( uv,cycle );
        uv *= 2.;
        cycle *= 2.;
    }

    return (f+1.)/2.;
}
