/*/
/*
/*  Spiral mesh display buffer
/*
/*/

// Display a spiral centered on (0,0)
float Spiral( vec2 xy, float beta, float gamma, int n)
{
    float rho = length(xy),
          theta = atan(xy.y,xy.x);
    
    // Find the parametrization
    float u = (logBase(beta,rho)-(theta+gamma))*float(n)/(2.*PI);
    return periodicDirac(u,1.,1e3 * rho / sqrt(float(n)));
}

void mainImage( out vec4 fragColor, vec2 fragCoord )
{
    vec3 color = BLACK;
    
    // Centered normalized coordinates in [-1,1]
    vec2 pixel = (2.*fragCoord-R.xy) / R.y;
    
    if(CLOCKWISE)
        pixel.y *= -1.;
    
    // Parameters of the spiral
    float beta=BETA,    // warp
    	  gamma=GAMMA;  // rotation
    int   n=N_GALAXY;   // number of arms
    
    ///////////////////////
    // Parameter for the orthogonal spirals
    float beta_orth = exp(-1./log(beta));
    
    // Number of spirals in the orthogonal mesh
    int n_orth = int(round(float(n)*1./log(beta)));
    
    float a;
    
    // White mesh
    float mesh = Spiral(pixel,beta_orth,0.,n_orth);
    color = mix(color,WHITE,mesh);
    
    a = mesh;
    
    // Colored spiral arms
    vec3[] colors = vec3[](RED,ORANGE,YELLOW,GREEN,TURQUOISE,BLUE);
    
    for(int arm=0; arm<n; arm++){
        float spiral = Spiral(pixel,beta,
                              gamma+float(arm)/float(n)*2.*PI,
                              1);
    	color = mix(color,colors[arm%colors.length()],spiral);
        a = max(a,spiral);
    }
    
    // Sharpen a and cut the center of the spiral
    a = POW3(a)*min(length(pixel)*10.,1.);
    
    fragColor = vec4(color,a);
}
