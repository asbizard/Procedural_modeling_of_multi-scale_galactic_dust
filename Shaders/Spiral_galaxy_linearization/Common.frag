
///////////////////////////////////
// Parameters presets for galaxies :
#define M51    	51
#define M101   	101
#define M74_1  	741
#define M74_2  	742
#define NGC3344	3344
#define M83     83
#define MWay    0
#define MWay2   1

#define GALAXY M51

/////////
// M51 //
/////////
#if GALAXY==M51
    // Warp
    #define BETA_GALAXY 1.38
    // Rotation
    #define GAMMA_GALAXY 1.95
    // Number of spiral arms
    #define N_GALAXY 2
    
    // Position of the center
	#define GALAXY_CENTER (vec2(250,185)/vec2(640,360))
    // Perspective distortion
	#define GALAXY_DISTORTION vec2(1.3,1)
    
    // Arms direction
	#define CLOCKWISE true
//////////
// M101 //
//////////
#elif GALAXY==M101
    #define BETA_GALAXY 1.52
    #define GAMMA_GALAXY 1.54
    #define N_GALAXY 4

	#define GALAXY_CENTER (vec2(315,177)/vec2(640,360))
	#define GALAXY_DISTORTION vec2(1.4,1)

	#define CLOCKWISE true
/////////
// M74 //
/////////
#elif GALAXY==M74_1
    #define BETA_GALAXY 1.32
    #define GAMMA_GALAXY 1.63
    #define N_GALAXY 2

	#define GALAXY_CENTER (vec2(310,183)/vec2(640,360))
	#define GALAXY_DISTORTION vec2(1.6,1)

	#define CLOCKWISE false
/////////
// M74 //
/////////
#elif GALAXY==M74_2
    #define BETA_GALAXY 1.32
    #define GAMMA_GALAXY 1.97
    #define N_GALAXY 2

	#define GALAXY_CENTER (vec2(205,205)/vec2(640,360))
	#define GALAXY_DISTORTION vec2(1.6,1)

	#define CLOCKWISE false
/////////////
// NGC3344 //
/////////////
#elif GALAXY==NGC3344
    #define BETA_GALAXY 1.22
    #define GAMMA_GALAXY 1.59
    #define N_GALAXY 2

	#define RMat(A) mat2(cos(A),-sin(A),sin(A),cos(A))

	#define GALAXY_CENTER (vec2(333,195)/vec2(640,360))
	#define GALAXY_DISTORTION vec2(1.5,1)

	#define CLOCKWISE false
/////////
// M83 //
/////////
#elif GALAXY==M83
    #define BETA_GALAXY 1.41
    #define GAMMA_GALAXY 0.62
    #define N_GALAXY 2

	#define GALAXY_CENTER (vec2(310,185)/vec2(640,360))
	#define GALAXY_DISTORTION vec2(1.8,1)

	#define CLOCKWISE false
///////////////
// Milky Way //
///////////////
#elif GALAXY==MWay
    #define BETA_GALAXY 1.25
    #define GAMMA_GALAXY 3.07
    #define N_GALAXY 2

	#define GALAXY_CENTER (vec2(320,185)/vec2(640,360))
	#define GALAXY_DISTORTION vec2(1.3,1)

	#define CLOCKWISE false
///////////////
// Milky Way //
///////////////
#elif GALAXY==MWay2
    #define BETA_GALAXY 1.25
    #define GAMMA_GALAXY 3.07
    #define N_GALAXY 2

	#define GALAXY_CENTER (vec2(325,173)/vec2(640,360))
	#define GALAXY_DISTORTION vec2(.87,.67)

	#define CLOCKWISE false
#endif

////////////////////////////////
// Final parameters

#define MANUAL false

#define BETA (MANUAL ? BETA_FROM_MOUSE : BETA_GALAXY)
#define GAMMA (MANUAL ? GAMMA_FROM_MOUSE : GAMMA_GALAXY)

////////////////////////////////
// Debug : Spiral parameters from mouse

#define BETA_FROM_MOUSE exp(POW3(iMouse.y/R.y)+.08)
#define GAMMA_FROM_MOUSE ((iMouse.x/R.x)*2.*PI)


///////////////////////////////////
// Various utils

#define POW2(A) ((A)*(A))
#define POW3(A) ((A)*(A)*(A))

#define PI 3.141593

#define BLACK		vec3(0)
#define WHITE		vec3(1)
#define GREY(A)		vec3(A)
#define RED  		vec3(1,0,0)
#define GREEN		vec3(0,1,0)
#define BLUE 		vec3(0,0,1)
#define TURQUOISE 	vec3(0,1,1)
#define YELLOW 		vec3(1,1,0)
#define LIGHT_BLUE	vec3(0,.5,1)
#define LAGOON		vec3(0,1,.5)
#define ORANGE		vec3(1,.5,0)

#define R iResolution

float logBase(float base, float x){
    return log(x)/log(base);
}

vec2 polToCart(float rho, float theta){
    return rho*vec2(cos(theta),sin(theta));
}

// Periodic function __|__|__|__|__
// s : dirac compression
float periodicDirac(float x, float period, float s){
	return pow(abs(cos(x*(PI/period))),s);
}

// Distance between a point p and a segment [p1,p2]
float distPointToSegment(vec2 p, vec2 p1, vec2 p2){
    float l = length(p2-p1);
    if(l==0.)
        return length(p-p1);
    else{
        // Segment unitary vector
        vec2 v = (p2-p1)/l;
        // Compute the projection of pos on the segment (clamped at segment edges)
        vec2 proj = p1 + v*clamp(dot(p-p1,v),0.,l);
        return length(p-proj);
    }
}

///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
////////////////////// Font printing lib //////////////////////
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

const int	_=32,
			_MINUS=45,		// -
			_DOT=46,		// .
			_0=48,
    		_EQ=61,			// =
			_A=65,	_B=66,	_C=67,	_D=68,	_E=69,
			_F=70,	_G=71,	_H=72,	_I=73,	_J=74,
			_K=75,	_L=76,	_M=77,	_N=78,	_O=79,
			_P=80,	_Q=81,	_R=82,	_S=83,	_T=84,
			_U=85,	_V=86,	_W=87,	_X=88,	_Y=89,
			_Z=90,
			_a=97,	_b=98,	_c=99,	_d=100,	_e=101,
			_f=102,	_g=103,	_h=104,	_i=105,	_j=106,
			_k=107,	_l=108,	_m=109,	_n=110,	_o=111,
			_p=112,	_q=113,	_r=114,	_s=115,	_t=116,
			_u=117,	_v=118,	_w=119,	_x=120,	_y=121,
			_z=122,
    		_beta=129,	_gamma=130, _theta=133,
            _DELTA=145,
    		_DEG=176;

struct PrintPosition{
    vec2 uv,
         pos;
    float scale;
};

struct PrintStyle{
    vec3 char_color,
         outline_color;
    float outline_size;
};

PrintStyle NewPrintStyle(vec3 col1, vec3 col2){
	return PrintStyle(col1,col2,.05);
}
#define DefaultPrintStyle NewPrintStyle(TURQUOISE,BLACK)

#define getDisplayPos(pos,scale) PrintPosition((fragCoord-pos*R.xy)/(scale*R.x),pos,scale);
#define nextLine(p)	p.pos.y -= scale*1.5,\
					p = getDisplayPos(p.pos,p.scale);

void writeChar(int char, PrintStyle style,
               float w, float x_offset, // Dimensions of the bbox of the char
               inout PrintPosition p, sampler2D font_tex, inout vec3 color){
    	#define getFont(uv,char) texture(font_tex, (uv+vec2(char%16,15-char/16))/16.)
		vec2 uv = p.uv;
    	uv.x += x_offset;
    	float outline_size = style.outline_size;
    	if(uv.x>0. && uv.x<w && uv.y>0. && uv.y<1.){
            // We are inside the bbox, display the char
            color = mix(color,style.outline_color,smoothstep(outline_size+.51,outline_size+.48,getFont(uv,char).a));
            color = mix(color,style.char_color, getFont(uv,char).r);
        }
		uv.x -= w; // move uv for next char
    	p.uv = uv;
}

const float default_ch_w = .8,
            default_ch_off = .3;

void writeStandardChar(int char, PrintStyle style,
                     inout PrintPosition p, sampler2D font_tex, inout vec3 color){
    writeChar(char,style,
              default_ch_w,default_ch_off,
              p,font_tex,color);
}

#define writeWord(word, style, p, font_tex, color)\
	for(int i_=0;i_<word.length();i_++)\
        writeStandardChar(word[i_],style,p,font_tex,color);



int powInt(int a, int b){
    int r = 1;
    for(int i=0;i++<b;r*=a);
    return r;
}

int intLog(int x, int base){
    if(x<1) return 0;
    int res = 0;
    for(;x>=base;res++)
        x /= base;
    return res;
}

void writeNumber(float number, int min_int_digits, int dec_digits,
                 PrintStyle style,
                 inout PrintPosition p, sampler2D font_tex, inout vec3 color){
	
    if(isnan(number)){
    	PrintStyle NaN_style = PrintStyle(BLACK,RED,.05);
        writeWord(int[](_N,_a,_N),NaN_style,p,font_tex,color);
    }
    else if(isinf(number)){
        PrintStyle Inf_style = PrintStyle(BLACK,LIGHT_BLUE,.05);
        writeWord(int[](_I,_n,_f),Inf_style,p,font_tex,color);
    }
    else{
        // Display the minus if number is negative
        if(number<0.)
            writeStandardChar(_MINUS,style,p,font_tex,color);
        
        // Round the number according to the number of decimal digits
        float decimal_digits_factor = float(powInt(10,dec_digits));
        int rounded_number = int(round(abs(number)*decimal_digits_factor));
        
        int int_part = rounded_number/int(decimal_digits_factor);
        int int_digits = 1 + intLog(int_part,10);
        // Fill with zeros to match min digits
        for(int i=0;i++< min_int_digits - int_digits ;)
            writeStandardChar(_0,style,
                              p,font_tex,color);
        
        
        int digits = int_digits+dec_digits;
        for(int x = powInt(10,digits);digits>0;digits--){
            if(digits==dec_digits)
                // Dot
                writeChar(_DOT,style,
                      .65,.45,
                      p,font_tex,color);
            writeStandardChar(_0+rounded_number/(x/=10),style,
                              p,font_tex,color);
            rounded_number%=x;
        }
    }
}
